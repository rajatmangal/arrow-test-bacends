CREATE TABLE IF NOT EXISTS user(
    id int AUTO_INCREMENT, 
    name VARCHAR(50), 
    email VARCHAR(50), 
    password VARCHAR(300), 
    token VARCHAR(255), 
    UNIQUE(email), 
    PRIMARY KEY(id)
);
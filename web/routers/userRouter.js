const express = require('express');

const router = new express.Router();
const userController = require('../controllers/userController');
const { checkAccessToken } = require('../middleware/authMiddleware');


router.post('/login', userController.post_login_service);

router.post('/refreshtoken', checkAccessToken, userController.post_refresh_token_service);

router.post('/signup', userController.post_signup_service);

router.delete('/logout', checkAccessToken, userController.delete_logout_service);

module.exports = router;
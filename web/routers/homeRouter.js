const express = require('express');

const router = new express.Router();
const homeController = require('../controllers/homeController');
const { checkAuthentication } = require('../middleware/authMiddleware');


router.get('/', homeController.get_home_service);

router.get('/private', checkAuthentication, homeController.get_private_service);

module.exports = router;
const passwordValidator = require("password-validator");
const logger = require("../setup/logger");

var validateUser = (res, user, req) => {
    if(req === "post" && (!user.name || typeof user.name !== 'string')) {
        createError(res, 400, "Invalid Name.");
        return false;
    }

    if(!user.email || typeof user.email !== 'string') {
        createError(res, 400, "Invalid Email.");
        return false;
    }

    if(!user.password) {
        createError(res, 400, "Invalid Password.");
        return false;
    }
    
    return true;
}

var createError = (res, status, error) => {
    logger.info("UserUtils: Error: " + error);
    return res.status(status).json({
        status: 'error',
        error: error
    });
}

var createResponse = (res, status, message, token, refreshToken) => {
    var response = {
        status: 'successful',
        message: message
    };
    if (token) {
        response.token = token;
    }
    if(refreshToken) {
        response.refreshToken = refreshToken;
    }
    return res.status(status).json(response);
}

var checkPassword = (password) => {
    var passwordCheck =  new passwordValidator();
    passwordCheck
        .has().uppercase()
        .has().lowercase()
        .has().symbols()
        .has().digits()

    return passwordCheck.validate(password);
}

module.exports = {
    validateUser,
    createError,
    createResponse,
    checkPassword
}
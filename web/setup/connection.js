const dotenv = require("dotenv");
const mysql = require("mysql");
const logger = require("./logger");

dotenv.config();

const connection = mysql.createConnection({
    host: process.env.MY_SQL_HOST,
    user: process.env.MY_SQL_USER,
    password: process.env.MY_SQL_PASSWORD,
    database: process.env.MY_SQL_DATABASE,
});

var connect = () => {
    connection.connect(function(error) {
        if(error) {
            logger.error("Connection.js: Error: " + error);
            throw error;
        } else {
            logger.info("Connection.js: Successfully connected to Database")
        }
    });
}

module.exports = {
    connect,
    connection
};
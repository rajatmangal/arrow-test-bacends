const { connection } = require('../setup/connection');

var getUser = (email, callback) => {
    let queryString = "SELECT * FROM user WHERE email = ?";
    connection.query(queryString, [email], (err, user) => {
        if(err){
            callback(false, err.sqlMessage);
        } else if(user.length === 0) {
            callback(false, "There is some problem with the given email/password.");
        }else {
            callback(true, user[0]);
        }
    });
}

var modifyUserToken = (modifiedValue, callback) => {
    let queryString = "Update user SET token = ? WHERE email = ?";
    connection.query(queryString, modifiedValue, (err, user) => {
        if(err){
            callback(false, err.sqlMessage);
        } else if(!user) {
            callback(false, "");
        }else {
            callback(true, "");
        }
    });
}

var postUser = (user, callback) => {
    let queryString = "INSERT INTO user SET ?";
    
    connection.query(queryString, user, (err, _row) => {
        if(err){
            if(err.sqlMessage.includes("Duplicate entry")) {
                callback("Email already taken");
            } else if(err.sqlMessage.includes("Data too long") && err.sqlMessage.includes("name")) {
                callback("Name too long");
            } else if(err.sqlMessage.includes("Data too long") && err.sqlMessage.includes("email")) {
                callback("Email too long");
            } else {
                callback(err.sqlMessage);
            }
        } else {
            callback(true);
        }
    })
}

module.exports = {
    getUser,
    modifyUserToken,
    postUser
}
# Simple Login System by Rajat Mangal
## Basic Outline
This project is a basic authenication system that can help users to register, login and logout from an application. This repo contains the back end side code only.

#
## Architecture

<center><img src="./assets/images/Architecture.jpg" alt="Application Architecture Model" style="margin-right: 10px;" /></center>
<br>

This app follows an 3-tier architecture i.e. Router layer that contains all the different routes that the application offers. Service layer is the business login layer. All the main checks and business code is present there. Then there is a Utils or Helper part. It is not a proper layer. It is just helping the service layer to provide help with very basic functions. The last layer is the Database layer. Every call to the database will be made from this layer. 

#
## Advatages of This Architecture
* Security is a big feature. We can secure the layers separately and can use different methods on different layers. 
* Easy to scale. If one tier needs more resources, we can simply scale that layer without impacting the other layer.
* Managing the layers become quite easy.
* It provides a lot of felixibility during development.

#
## Auth Architecture Model
In this application, I am using Json Web Tokens to create new tokens everytime a user logs in. But I am generating 2 tokens. One is the basic access token and other one is the refresh token. I will explian how it works with this diagram below:

<center><img src="./assets/images/Auth Arch.jpg" alt="Auth Architecture Model" style="margin-right: 10px;" /></center>

So, in the diagram, we have 2 servers. One is the Authorization server and the other one is Resource Server. Authorization Server is the one that will help user to register, login and logout. The one I built in this application. Resource server will have all the other endpoint (both secured and unsecured). I have not made a separate resource server for this application as it is beyond the scope of this assignment. I have adde 2 endpoints from resource in the same server. But we will craete a separate resource server for the real application. This is how it will work. If a user signs in, the request will go to the Authorization Server and it will return an Access Token and a Refresh Token. Now Access Token is the one that will be sent with every request that is made for a secure endpoint. But it will have a short life span. Refresh Token will have a large life span. Now once the access token is expired, the front end will send a request to the Authorixation server to update the access token and refresh token will be sent along with it. The server will check if the refresh token is active. If yes, it will update the access token and send back. Otherwise, it will ask user to sign in again. For logout, I am simply removing the refresh token from the db and the access token will be removed from the front end server. The main reason that I am using this approach is that <b>it adds a layer to protection</b>. If in some case, the access stoken in compromized, attacker will have less time to attack the system (short lived token).

#
## Link for the ednpoints
http://localhost:3000/

#
## Routes
* /login<br>
<b>Header</b>: None<br>
<b>Body:</b> Email and Password in JSON Format<br>
<b>Returns:</b><br>
200: Successful (Sends a Successful message along with access and refresh token)<br>
400: Failed (Multiple Reason: No Email, password or If user does not exist in db)<br>
401: Failed (If the password is wrong)<br>
500: Failed (Fails to store the token in db)


* /signup<br>
<b>Header</b>: None<br>
<b>Body:</b> Name, Email and Password in JSON Format<br>
<b>Returns:</b><br>
200: Successful (Sends a Successful message back)<br>
400: Failed (Multiple Reason: No Name, Email, password sent or password requirements not satisfied.)<br>
500: Failed (SQL Related issues)


* /refreshtoken<br>
<b>Header</b>: x-auth-token<br>
<b>Body:</b> None<br>
<b>Returns:</b><br>
200: Successful (Sends a Successful message back)<br>
403: Failed (If the access token is wrong, not present or user already logged out.)

* /logout<br>
<b>Header</b>: x-auth-token<br>
<b>Body:</b> None<br>
<b>Returns:</b><br>
200: Successful (Sends a Successful message back)<br>
400: Failed (If user can't be updated with the given token in the database)<br>
403: Failed (If the access token is wrong or not present)

* /home<br>
<b>Header</b>: None<br>
<b>Body:</b> None<br>
<b>Returns:</b><br>
200: Successful (Sends a Successful message back)


* /home/private<br>
<b>Header</b>: x-auth-token<br>
<b>Body:</b> None<br>
<b>Returns:</b><br>
200: Successful (Sends a Successful message back)<br>
400: Failed (If the token is not present)<br>
401: Failed (If the access token is Invalid)<br>
<br>

#
## How to test:
I have included a Postman collection along with the application. You can use that to test the application. 
<br>Note:</br> Please add 1000ms time delay while running the tests. Without the delay, it might interfare with the order of the test.

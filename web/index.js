const express = require("express");

const mySqlConnection = require("./setup/connection");
const userRouter = require('./routers/userRouter');
const homeRouter = require('./routers/homeRouter');

const port = process.env.PORT || 3000;

mySqlConnection.connect();

const app = express();

app.use(express.json());

app.use('/', userRouter);

app.use('/home', homeRouter);

app.get("*", (req, res) => {
	res.send("404. Please check the list of routes");
});

app.listen(port, ()=> {
    console.log("App running at port 3000");
})
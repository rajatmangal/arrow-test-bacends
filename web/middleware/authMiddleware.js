const jwt = require('jsonwebtoken');
const jwtSec = process.env.JWT_SECRET;

const helper = require('../utils/userUtils');


var checkAuthentication = (req, res, next) => {
    let token = req.body.token || req.headers["x-access-token"];
    if(!token) {
        return helper.createError(res, 401, "Token is Invalid.")
    }
    try {
        let decodeToken = jwt.verify(token, jwtSec);
        req.user = decodeToken;
        req.user.token = token;
    } catch(err) {
        return helper.createError(res, 401, "Token is Invalid.")
    }
    next();
}

var checkAccessToken = (req, res, next) => {
    const refreshToken = req.header("x-auth-token");

    if(!refreshToken) {
        return helper.createError(res, 403, "Refresh Token required for this request.");
    }

    req.refreshToken = refreshToken;
    
    next();
}

module.exports = {
    checkAuthentication,
    checkAccessToken
}
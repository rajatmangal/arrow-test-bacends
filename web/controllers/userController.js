const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const userDbo =  require('../dbo/userDbo');
const helper = require('../utils/userUtils');
const logger = require('../setup/logger');

const JWTAccessSec = process.env.JWT_ACCESS_SECRET;
const jwtAccessExpiry = process.env.JWT_ACCESS_EXPIRY;

const JWTRefreshSec = process.env.JWT_SECRET;
const jwtRefreshExpiry = process.env.JWT_EXPIRY;

const saltRounds = 10;

var post_login_service = (req, res) => {
    logger.info("UserController: Post request on /login received by user: " + req.body.email + ".");
    let user = req.body;

    if(helper.validateUser(res, user, 'get')) {
        userDbo.getUser(user.email, async function(userFound, val) {
            if(userFound === true) {
                if(await bcrypt.compare(user.password, val.password)) {
                    const token = jwt.sign(
                        {
                            id: val.id,
                            email: val.email
                        }, JWTAccessSec, { expiresIn: jwtAccessExpiry }
                    )
                    const refreshToken = jwt.sign(
                        {
                            id: val.id,
                            email: val.email
                        }, JWTRefreshSec, { expiresIn: jwtRefreshExpiry }
                    )
                    userDbo.modifyUserToken([refreshToken, val.email], function(updateUser, message) {
                        if(updateUser === true) {
                            return helper.createResponse(res, 200, "Logged in Successfully", token, refreshToken);
                        } else {
                            return helper.createError(res, 500, "Failed to store the token in the db. Please try login again.");
                        }
                    });
                } else {
                    return helper.createError(res, 401, "There is some problem with the given email/password.")
                }
            } else {
                helper.createError(res, 400, val);
            }
        })
    }

};

var post_refresh_token_service = (req, res) => {
    logger.info("UserController: Post request on /refreshToken received.");
    try {
        let user = jwt.verify(req.refreshToken, JWTRefreshSec);
        userDbo.getUser(user.email, (userFound, val) => {
            if(userFound === true && val.token != "") {
                let token = jwt.sign(
                    {
                        id: val.id,
                        email: val.email
                    }, JWTAccessSec, { expiresIn: jwtAccessExpiry }
                );
                return helper.createResponse(res, 200, "Access Token Created.", token, req.refreshToken);     
            } else {
                return helper.createError(res, 403, "User Logged Out. Please login again.")
            }
        });
    } catch {
        return helper.createError(res, 403, "Invalid Token. Please Login Again.");
    }

};

var post_signup_service = async (req, res) => {
    logger.info("UserController: Post request on /signup received.");
    let user = req.body;

    if(helper.validateUser(res, user, 'post')) {
        if(user.password.length < 5) {
            return helper.createError(res, 400, "Password should have atleast 5 characters.")
        }
    
        if(user.password.length > 20) {
            return helper.createError(res, 400, "Password should have a max of 20 characters.")
        }
    
        if(!helper.checkPassword(user.password)) {
            return helper.createError(res, 400, "Password is too weak. Please try and include atleast one of each : a lowercase letter, an uppercase letter, a symbol and a number.")
        }
    
        user.password = await bcrypt.hash(user.password, saltRounds);
    
        userDbo.postUser({
            name: user.name, 
            email: user.email, 
            password: user.password
        }, function(insertUser) {
            if(insertUser === true) {
                helper.createResponse(res, 200, "User Created Successfully")
            } else{
                helper.createError(res, 500, insertUser);
            }
        });
    
        
    };
};

var delete_logout_service = (req, res) => {
    logger.info("UserController: Delete request on /logout received.");
    try {
        let user = jwt.verify(req.refreshToken, JWTRefreshSec);
        userDbo.modifyUserToken(["", user.email], function(updateUser, message) {
            if(updateUser === true) {
                return helper.createResponse(res, 204, "Logged Out Successfully.");
            } else {
                return helper.createError(res, 400, "Failed to log out. Please try again later.");
            }
        });
    } catch {
        return helper.createError(res, 403, "Invalid Token. Please Login Again.");
    }
};

module.exports = {
    post_login_service,
    post_refresh_token_service,
    post_signup_service,
    delete_logout_service
}

const helper = require('../utils/userUtils');
const logger = require('../setup/logger');

var get_home_service = (req, res) => {
    logger.info("HomeController: Get request on /home received.");
    return helper.createResponse(res, 200, "Welcome to Simple Login System.");
}

var get_private_service = (req, res) => {
    logger.info("HomeController: Get request on /home/private received.");
    return helper.createResponse(res, 200, `Welcome ${req.user.email} to the private page.`)
}

module.exports = {
    get_home_service,
    get_private_service
}
